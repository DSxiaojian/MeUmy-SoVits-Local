# AI MeUmy  SoVits 模型 本地版

## 项目简介

1. 采用 **[so-vits-svc](https://github.com/innnky/so-vits-svc)** 项目进行模型训练 咩栗v1.0采样率为32khz 呜米v1.0、咩栗v2.0采样率为44.1khz
2. 采用 [咩栗](https://space.bilibili.com/745493/) 与 [呜米](https://space.bilibili.com/617459493/) 的声音制作的数据集
3. 为了方便使用已将模型封装成 [MoeSS](https://github.com/NaruseMioShirakana/MoeSS) 版本
4. 模型训练与数据集制作 [DS绝对小贱](https://space.bilibili.com/17467266/) ,模型有问题可以在B站私信我
5. 在线版（仅咩栗 v1.0模型）请前往[点击打开](https://huggingface.co/spaces/DSXiaoJian/MeUmy-SoVits)
6. pth文件下载链接：https://pan.baidu.com/s/10LGPAaGmikkaa5ai8IQ57Q?pwd=dkd0 提取码：dkd0

## 使用方法

### **注！咩栗v1.0模型为 SoVits 3.0  呜米v1.0、咩栗v2.0模型为 SoVits 4.0**

1. 请前往 项目 [MoeSS](https://github.com/NaruseMioShirakana/MoeSS) `Releases` 中下载最新版 `MoeSS-CPU.7z` 并解压出来

2. 下载 [MoeSS](https://github.com/NaruseMioShirakana/MoeSS) 中的前置模型 [hubert.7z](https://huggingface.co/NaruseMioShirakana/MoeSS-SUBModel/resolve/main/hubert.7z) 解压放到`MoeSS-CPU\hubert`文件夹中（3.0的前置模型 使用4.0请忽略此步骤）

3. 下载 [MoeSS](https://github.com/NaruseMioShirakana/MoeSS) 中的前置模型 [hubert4.0.7z](https://huggingface.co/NaruseMioShirakana/MoeSS-SUBModel/resolve/main/hubert4.0.7z) 解压放到`MoeSS-CPU\hubert`文件夹中

4. 下载本项目`发行版`中最新模型文件，**请注意压缩包为分卷压缩请一定完全下载**

5. 解压模型文件放到`MoeSS-CPU\Mods`文件夹中

    ![](https://gitee.com/DSxiaojian/MeUmy-SoVits-Local/raw/master/img/mods.png)

6. 打开`MoeSS.exe`选择**模型**和**角色**后点击**开始转换语音**选择要被替换音色的**.wav**文件即可

7. 具体MoeSS使用方法可以参考 [MoeSS使用方法](https://github.com/NaruseMioShirakana/MoeSS)


## 注意

1. 使用男性音频可以考虑使用 升降调+4或+6，使用女性音频可以不做调整。
2. 使用该模型请遵循[MoeSS用户协议](https://github.com/NaruseMioShirakana/MoeSS)
3. 使用该模型请标注作者 **模型训练/数据集:DS绝对小贱**
4. 语音模型转换出的音频请勿用于商业化。

## 更新

- 2023.2.17 AI咩栗 v1.0 正式上线
- 2023.2.26 AI呜米 v1.0 正式上线
- 2023.3.18 AI咩栗 v2.0 正式上线
- 2023.5.19 提供pth文件下载

## 后续更新内容

- AI蜗牛姐姐（看情况更新）
- AI呜米妹妹（看情况更新）